#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/10 23:00
# @Author  : Feng Xiaoqing
# @File    : demo2.py
# @Function: -----------

age = input("Please input your age: ")
# print(type(age))
if age.strip():
    if age.isdigit():
        if int(age)>=18:
            print("你是一个成年人！")
        else:
            print("你还是一个小屁孩！")
    else:
        print("你输入的不是数字")
else:
    print("你输入的只有空格")
