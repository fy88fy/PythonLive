#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/4 22:59
# @Author  : Feng Xiaoqing
# @File    : re-to-file.py
# @Function: -----------
import re,os,codecs
#下面生成upstream内容
regUpstream = re.compile(r"\s*(upstream\s+(\S+)\s+{[^}]+})") #正则匹配
with codecs.open("ga10.wms5.jd.com.txt") as fu:
    textList = regUpstream.findall(fu.read())
    if not os.path.exists("upstream"):            #判断upstream文件夹是否存在，不存在则建立
        os.mkdir("upstream")
    os.chdir("upstream")
    for item in textList:
        with codecs.open(item[1], "w") as fw:     #生成以item[1]为名字的文件，并把匹配到的内容写入文件中
            fw.write(item[0])
    os.chdir("..")


#下面生成location内容
regLocation = re.compile(r"(location\s+/(\S+)/\s+{\s+(proxy_next_upstream)?.*[^}]*?})")  #正则匹配

with codecs.open("ga10.wms5.jd.com.txt") as fl:
    textLocation = regLocation.findall(fl.read())
    if not os.path.exists("location"):            #判断location文件夹是否存在，不存在则建立
        os.mkdir("location")
    os.chdir("location")
    for each in textLocation:
        file = each[1] + ".locaion.conf"          #生成以文件名，并把匹配到的内容写入文件中
        with codecs.open(file, "w") as flw:
            flw.write(each[0])