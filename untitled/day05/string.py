#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/10 22:13
# @Author  : Feng Xiaoqing
# @File    : string.py
# @Function: -----------
"""
输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。
1.程序分析：利用while语句,条件为输入的字符不为'\n'.
#用isdigit函数判断是否数字
#用isalpha判断是否字母
#isalnum判断是否数字和字母的组合

输入一个字符串，判断其中串数字、字母、空格及其他字符分别有多少个？

"""




while 1:
    strings = input("Please input a string(quit will be exit):")
    alpha, dig, space, other = 0, 0, 0, 0
    if strings.strip() == "quit":
        exit(1)
    for i in strings:
        if i.isdigit():
            dig += 1
        elif i.isspace():
            space += 1
        elif i.isalpha():
            alpha += 1
        else:
            other += 1
    print("alpha = {0}".format(alpha))
    print("dig = {0}".format(dig))
    print("space = {0}".format(space))
    print("other = {0}".format(other))