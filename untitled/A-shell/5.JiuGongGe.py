#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/11 23:16
# @Author  : Feng Xiaoqing
# @File    : 5.JiuGongGe.py
# @Function: -----------

"""
                -------------
                | A | B | C |
                | D | E | F |
                | G | H | I |
                -------------
所有的横竖斜线加起来都等于15
"""

count=0
list=[0,1,2,3,4,5,6,7,8,9]
for A in list:
    a=list.copy()
    a.remove(A)
    for B in a:
        b = a.copy()
        b.remove(B)
        for C in b:
            c = b.copy()
            c.remove(C)
            for D in c:
                d= c.copy()
                d.remove(D)
                for E in d:
                    e = d.copy()
                    e.remove(E)
                    for F in e:
                        f = e.copy()
                        f.remove(F)
                        for G in f:
                            g = f.copy()
                            g.remove(G)
                            for H in g:
                                h = g.copy()
                                h.remove(H)
                                for I in h:
                                    if (A+B+C)==(D+E+F)==(G+H+I)==(A+D+G)==(B+E+H)==(C+F+I)==(A+E+I)==(G+E+C)== 15:
                                        count += 1
                                        print("""                                       
                                                        ---第{9}种----
                                                        | {0} | {1} | {2} |
                                                        | {3} | {4} | {5} |
                                                        | {6} | {7} | {8} |
                                                        -------------                                                                               
                                        """.format(A,B,C,D,E,F,G,H,I,count))



