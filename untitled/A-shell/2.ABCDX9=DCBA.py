#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/11 22:14
# @Author  : Feng Xiaoqing
# @File    : 2.ABCDX9=DCBA.py
# @Function: -----------

for A in range(1,10):
    for B in range(0,10):
        for C in range(0,10):
            for D in range(0,10):
                if (A*1000+B*100+C*10+D)*9==D*1000+C*100+B*10+A :
                    print("A={0}".format(A))
                    print("B={0}".format(B))
                    print("C={0}".format(C))
                    print("D={0}".format(D))
                    print("{0}{1}{2}{3}x9={3}{2}{1}{0}".format(A,B,C,D))
