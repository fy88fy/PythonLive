#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/11 22:24
# @Author  : Feng Xiaoqing
# @File    : 3.PanDuan_strings.py
# @Function: -----------
num_dig=0
num_alph=0
num_space=0
num_other=0
while 1:
    strings=input("Please input string:")
    if strings=="quit":
        exit(1)
    for i in strings:
        if i.strip().isdigit():
            num_dig += 1
        elif i.strip().isalpha():
            num_alph += 1
        elif i.isspace():
            num_space += 1
        else:
            num_other += 1
    print("数字个数为：{0}".format(num_dig))
    print("字母个数为：{0}".format(num_alph))
    print("空格个数为：{0}".format(num_space))
    print("其他字符个数为：{0}".format(num_other))



