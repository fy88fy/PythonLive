#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/11 22:48
# @Author  : Feng Xiaoqing
# @File    : 4.JieCheng_for_n.py
# @Function: ----------

n=input("Please input a number n: ")
result = 1
num = 1
for i in range(1,int(n)+1):
    num *=i
    result += num
print("{0}的阶乘为：{1}".format(n,result))