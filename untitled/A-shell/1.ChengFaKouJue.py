#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/11 22:12
# @Author  : Feng Xiaoqing
# @File    : 1.ChengFaKouJue.py
# @Function: -----------

"""
99乘法口决 Python3
"""

for i in range(1,10):
    for j in range(1,i+1):
        print("{0}x{1}={2} ".format(j,i,i*j),end="")
    print()