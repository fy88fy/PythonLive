#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/18 23:32
# @Author  : Feng Xiaoqing
# @File    : test.py
# @Function: -----------


def startEnd(author):
    def a(fun):
        def b(name):
            print("this author is {0}".format(author))
            print("start")
            fun(name)
            print("end")
        return b
    return a

#@startEnd("fengxiaoqing")
def hello(name):
    print("hello {0}".format(name))

hello("fxq")