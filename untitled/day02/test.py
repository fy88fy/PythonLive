#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/3/29 21:12
# @Author  : Feng Xiaoqing
# @File    : test.py
# @Function: -----------

# "".strip()  字符串过滤空格
# "".replace(old,new) 字符串老替换新的
# "".find(sub) 字符查找sub字符的内容，如果找到，返回字符串的下标  否则返回-1
# "{0} {1} {2}".format(name1,name2,name3) 字符串格式化
# "".startswith()
# "----".join("abc")   以a分隔
'''

注释
'''

"""
注释

"""

str1= "a1b2c3d4e5"
print(str1[0])
print(str1[1])
print(str1[2])
print(str1[3])
print(str1[4])
print(str1[5])
print("----".join(str1))


