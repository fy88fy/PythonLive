#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/29 22:02
# @Author  : Feng Xiaoqing
# @File    : test.py
# @Function: -----------

import re

import ti

print(timeit.timeit(setup='''import re; reg = re.compile('<(?P<fxqname>\w*)>.*</(?P=fxqname)>')''', stmt='''reg.match('<h1>xxx</h1>')''', number=1000000))

print(timeit.timeit(setup='''import re''', stmt='''re.match('<(?P<fxqname>\w*)>.*</(?P=fxqname)>', '<h1>xxx</h1>')''', number=1000000))


reg = re.compile('<(?P<fxqname>\w*)>.*</(?P=fxqname)>')

reg.match('<h1>xxx</h1>')



re.match('<(?P<fxqname>\w*)>.*</(?P=fxqname)>', '<h1>xxx</h1>')