#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/2 17:13
# @Author  : Feng Xiaoqing
# @File    : random.py
# @Function: -----------
import random
random.random()
'''
>>> import random
>>> random.random()
0.11941491373239144

>>> random.randint(10,20)  #取整数
18

>>> random.uniform(10,200)  #取浮点数
42.640506720426892

>>> random.randrange(10,200,2)  #取12，14，16。。。到200的随机数
76

>>> random.sample("1345666",4)    #随机取4个长度
['3', '4', '6', '6']
'''