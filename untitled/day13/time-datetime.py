#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/2 15:44
# @Author  : Feng Xiaoqing
# @File    : time-datetime.py
# @Function: -----------

import os
import sys

import time,datetime

print(time.mktime(time.localtime()))   #打印当前时间
print(time.time())   #打印当前时间戳
# time.sleep(5)    # sleep 5s
print(time.asctime())
print(time.ctime())

print("########################")

print(datetime.datetime.now().year)
print(datetime.datetime.now().month)
print(datetime.datetime.now().day)
print(datetime.datetime.now().strftime("%Y-%m-%d"))

'''
%Y 带世纪部分的十制年份
%m 十进制表示的月份
%d 十进制表示的每月的第几天
%H 24小时制的小时
%M 十时制表示的分钟数
%S 十进制的秒数
%c  标准时间，如：04/25/17 14:35:14  类似于这种形式
%a   本地（locale）简化星期名称
%b	本地简化月份名称
%d	一个月中的第几天（01 - 31）
'''


from datetime import datetime, timedelta
now_time = datetime.now()
print(now_time)     #打印现在的时间
b = now_time + timedelta(days=-1, weeks=-1)
print(b)    #打印一周前的时间

#时间转换为字符串格式
string = '2017-04-25 11:59:58'
time1 = datetime.strptime(string, '%Y-%m-%d %H:%M:%S')
print(time1)


#时间戳转时间对象
t = time.time()
dt = datetime.fromtimestamp(t)
print(type(dt))		#<type 'datetime.datetime'>





