# !/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/29 22:55
# @Author  : Feng Xiaoqing
# @File    : re.py
# @Function: -----------
import re

line = "Cats are smarter than dogs"

matchObj = re.match(r'(.*) are (.*?) .*', line, re.M | re.I)

if matchObj:
    print
    "matchObj.group() : ", matchObj.group()
    print
    "matchObj.group(1) : ", matchObj.group(1)
    print
    "matchObj.group(2) : ", matchObj.group(2)
else:
    print
    "No match!!"
