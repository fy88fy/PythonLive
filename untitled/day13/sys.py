#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/2 17:02
# @Author  : Feng Xiaoqing
# @File    : sys.py
# @Function: -----------


import  sys
print(sys.argv[0])   #获取文件名
# print(sys.argv[1])   #获取第一个参数
# print(sys.argv[2])   #获取第二个参数

f_handler=open('out.log', 'w')
sys.stdin=f_handler     #直接输出到屏幕中
print('hello222')
print('hello222333')


f_handler=open('out.log', 'w')
sys.stdout=f_handler     #直接输出到文件中
print('hello222')
print('hello222333')



print('this error')
sys.stderr.write("this is a error message")  #错误输出