#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/2 16:15
# @Author  : Feng Xiaoqing
# @File    : os.py
# @Function: -----------
'''
目录和文件操作
os.getcwd()
os.chdir(“/export”)
os.listdir(“/”)
os.mkdir(“aaa”)
os.remove(“1.txt”)
os.path.exists(‘目录’)	判断目录是否存在
os.linesep	打印操作系统的分隔符，linux系统的分隔符\n，windows系统的分隔符\r\n，mac系统的分隔符\r
os.path.splitext(‘文件’)	把文件的后缀名和前面分开，返回一个tuple
os.path.split(‘文件或者目录’)	把最后的一个目录或者文件和前面的目录分开，返回一个tuple


os.path.join(os.getcwd(), 'aaa', ‘bbb’, ‘ccc’)	拼接出来多级目录：E:\test\aaa\bbb\ccc

'''
import os

# os.system("dir")   #Pycharm中，中文有乱码
# aaa = os.popen("ipconfig").read()  #中文无乱码
# print(aaa)

os.chdir('d:/testdir')    #进入目录
print(os.getcwd())  #  显示当前目录
# os.remove("1.txt")#   删除文件

print(os.listdir('D:/PycharmProjects/PythonLive/untitled/'))  #显示目录文件

# os.mkdir("testdir")   #创建目录
print(os.path.exists("testdir"))  #判断目录是否存在

print(os.linesep)
print(os.path.splitext("2.txt"))   # 分隔文件名和扩展名('2', '.txt')
print(os.path.split("D:/test/111/1.txt"))     #分隔目录和文件：('D:/test/111', '1.txt')
print(os.path.join("D:\\","aaa","bbb","ccc"))  #创建多级目录 D:\aaa\bbb\ccc


#linux独有模块commands
'''
>>> import commands
>>> print(commands.getoutput("ls"))
anaconda-ks.cfg
copy-files
expect
fxq-pssh.tar.gz
pyinstall
shell
test.txt



>>> print(commands.getstatusoutput("ls"))
(0, 'anaconda-ks.cfg\ncopy-files\nexpect\nfxq-pssh.tar.gz\npyinst    #第一个0为返回状态，为成功
all\nshell\ntest.txt')>>> 

'''








