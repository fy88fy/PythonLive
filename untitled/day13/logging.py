#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/5/2 16:05
# @Author  : Feng Xiaoqing
# @File    : logging.py
# @Function: -----------
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',datefmt=' %Y/%m/%d %H:%M:%S', filename='myapp.log', filemode='w')
logger = logging.getLogger(__name__)
logging.debug('This is debug message')
logging.info('This is info message')
logging.warning('This is warning message')
