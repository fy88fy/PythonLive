#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/1 21:56
# @Author  : Feng Xiaoqing
# @File    : test.py
# @Function: -----------

# n = 0
# while 1:
#     if n == 10:
#         break
#     print(str(n)+" hello")
#     n += 1

# sth=''
# while sth != 'q':
#     sth=input("Please input sth,q for exit:")
#     if not sth:
#         break
#     if sth == 'quit':
#         continue
#         print ('continue')
# else:
#     print ('GOOD BYE')

#!/usr/bin/python

for i in range(1,10):

	for j in range(1,i+1):

		print("%sx%s=%s" % (j,i,j*i),end=" ")

	print()
