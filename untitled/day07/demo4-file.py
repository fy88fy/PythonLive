#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/16 21:49
# @Author  : Feng Xiaoqing
# @File    : demo4-file.py
# @Function: -----------
#
# #读文件
# def readFile():
#     f = open("file")
#     line = f.read()
#     print(line)
#     f.close()
# readFile()
#
def rFile():
    with open("file") as f:
        for i in f.readlines():
            print(i)
rFile()
#
# #日历
# import calendar
# cal = calendar.month(2016, 1)
# print("以下输出2016年1月份的日历:")
# print(cal)
#
# #时间
# import datetime
# i = datetime.datetime.now()
# print ("当前的日期和时间是 %s" % i)
# print ("ISO格式的日期和时间是 %s" % i.isoformat() )
# print ("当前的年份是 %s" %i.year)
# print ("当前的月份是 %s" %i.month)
# print ("当前的日期是  %s" %i.day)
# print ("dd/mm/yyyy 格式是  %s/%s/%s" % (i.day, i.month, i.year) )
# print ("当前小时是 %s" %i.hour)
# print ("当前分钟是 %s" %i.minute)
# print ("当前秒是  %s" %i.second)
#
#
# #写文件
# def writeFile():
#     import datetime
#     import time
#     i = datetime.datetime.now()
#     f = open("logs/file","a")
#     f.write("222222222")
#     f.write("\n")
#     f.close()
# writeFile()


#写文件
def wFile():
    with open("file","a") as f:
        f.write("bbbbb\n")
wFile()





