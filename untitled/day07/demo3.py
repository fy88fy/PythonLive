#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/15 22:47
# @Author  : Feng Xiaoqing
# @File    : demo3.py
# @Function: -----------

#
# m = dict(a=1, c=10, b=20, d=15)
# print(sorted(m.items(), key = lambda d:d[1],reverse = True)) #按value值倒序排列



a = [x*x for x in range(1,11) if x%2 ==0]
print (a)
print(type(a))

b = (x*x for x in range(1,11) if x%2 ==0)
print (b)
print(type(b))

for i in b:
    print(i)
print('#'*20)


