#!/usr/bin/env python
# -*- coding:utf-8 -*-

# def f(x):
#     return x*x
#
# for i in map(f,[1,2,3,4,5,6,7]):
#     print(i)
# from functools import reduce
#
# def f(x,y):
#     return x+y
# print(reduce(f,[1,2,3,4,5,6,7,8,9,10]))
# print(reduce(f,range(1,101)))


for i in filter(lambda x:x<7, [1, 2, 3, 4, 5,40,8]):
    print(i)
