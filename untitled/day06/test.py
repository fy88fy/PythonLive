#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/13 23:41
# @Author  : Feng Xiaoqing
# @File    : test.py
# @Function: -----------

# f = open("test.log","a",encoding='utf-8')
# f.write("Hello World!\n")
# f.close()

class File():
    ENCODING = "utf-8"
    def wirteFile(self):
        filename = input("Please input the file name: ")
        f = codecs.open(filename,'a',encoding=File.ENCODING)
        while 1:
            context = input("Please input the file context(quit for exit): ")
            if context == "quit":
                break
            f.write(context)
            f.write("\n")
    def readFile(self):
        print("####################STAT######################")
        readfile = input("Please input your read file name: ")
        fileread = codecs.open(readfile,encoding=File.ENCODING)
        readContext = fileread.read()
        print(readContext)
        print("################### END ######################")
        fileread.close()
if __name__ == '__main__':
    import codecs
    File = File()
    File.wirteFile()
    File.readFile()


